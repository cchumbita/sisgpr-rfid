const app = require('express')()
const express = require('express')
const http = require('http')
const mongoose = require('mongoose')
const SocketIO = require('socket.io')
const { exec } = require('child_process')
const cors = require('cors')
const SerialPort = require('serialport')
const moment = require('moment-timezone')
const request = require('request-promise')
const Readline = require('@serialport/parser-readline')
const fetch = require('node-fetch')
let api_url = "http://apiv1-test1.sisgpro.com/presentisms"


app.set('port', process.env.PORT || 8085)
const server = app.listen(app.get('port'), () => {
    console.log('Socket.IO in port', app.get('port') )
})
const io = SocketIO(server)


const corsOptions = {
  origin: '*',
  methods: '*'
}
app.use(cors(corsOptions))


// SCHEMA

const Schema = mongoose.Schema;

const PresentismSchema = new mongoose.Schema({
    card_number: {
        type: String,
        required: true
    },
    datetime:{
        type: String,
        required: true
    },
    send: {
        type: Boolean,
        default: false
    }
  },{
    timestamps: true
    }
)
let Presentism = mongoose.model('Presentism', PresentismSchema);

const LastPorts = new mongoose.Schema({
    port: {
        type: String,
        required: true
    }
  },{
    timestamps: true
    }
)
let lastPorts = mongoose.model('LastPorts', LastPorts);

// Que sistema operativo tengo?
const SO = process.platform
let run = false
let device = "NONE"
let statusDevice = "NONE"
let port = null


// Obtengo los dispositivos conectados
const getDeviceList = ()=>{
    return new Promise((resolve, reject) => {
        if( SO.toLowerCase() === "linux" ){
            exec('ls /dev/*AC*', (error, stdout, stderr) => {
                if (error) return resolve([])
                if (stderr) return resolve([])
                let devices = stdout.split("\n")
                devices = devices.filter(el=>{
                    if(el!= "") return true
                    return false
                })
                resolve(devices) 
            })
        }else if(SO.toLocaleLowerCase() === 'win32'){
            exec('wmic path Win32_SerialPort get Caption | findstr Arduino', (error, stdout, stderr) => {
                if (error) return resolve([])
                if (stderr) return resolve([])
                let devices = stdout.split("\n")
                devices = devices.filter(el=>{
                    if(el!= "") return true
                    return false
                })
                let regExp = /\(([^)]+)\)/
                devices = devices.map(el=>{
                    let r = regExp.exec(el)
                    return r[1]
                } )
                resolve(devices) 
            })
        }
    })
}

const saveLastPort = (port) => {
    return new Promise(async(resolve, reject) => {
        lastPorts.create({port: port}, function (err, small) {
            if (err) return reject(err)
                resolve(true)
            })
            
    })
}

const getLastPort =  ()=>{
    return new Promise(async(resolve, reject) => {
        await lastPorts.findOne({},null,{sort: {createdAt: -1 }}, (error, data)=>{
            if (error) return reject(false)
            if(data){
                device = data.port
                conectarDispositivo()
            }
        })
    })
}

async function sendTrue(id)
{
    await Presentism.findOneAndUpdate({_id: id},{$set:{send: true}}, (e, d)=>{
        if (e) return false
        return true
    })
}

function enviarDatos()
{

        Presentism.find({send: false}, (err, data)=>{
            if(err) console.log(err)
            if(data){
                io.emit('LOG', "Enviando " + parseInt("0"+data.length))
                console.log("Enviando " + parseInt("0"+data.length))
                Object.keys(data).forEach(async function(key) {
                    const body = { card_number: data[key].card_number, datetime: data[key].datetime };
 
                    fetch(api_url, {
                            method: 'post',
                            body:    JSON.stringify(body),
                            headers: { 'Content-Type': 'application/json' },
                        })
                        .then(res => res.json())
                        .then(json => console.log(json));
                    // await request.post(api_url, {form:{}}, async function(err,httpResponse,body){ 
                        // if (err){
                        //     console.log(httpResponse)
                            
                        // }
                        // $result  = await sendTrue(data[key]._id)
                    // })
                })
            }
        })


}


async function conectarDispositivo()
{
    try {
        port = await new SerialPort(device, async (err)=>{
            if(err){
                statusDevice = "Fallo la conexion"
                console.log("1-Error al conectar con dispositivo. " + err)
                return false
            }
            await saveLastPort(device)
            statusDevice = "Conectado"
            console.log('Se conecto exitosamente')
            
        })
        let parser = port.pipe(new Readline({ delimiter: '\r\n' }))
        parser.on('data', function(data){
            if(data.substring(0, 9)==='Card UID:'){
                Presentism.create({card_number: data.replace("Card UID: ", ""), datetime: moment()}, function (err, small) {
                if (err) return handleError(err)
                    io.emit('NEW_RECORD', {card_number: data.replace("Card UID: ", ""), datetime: moment()})
                    console.log('Nuevo  registro. ' + data.replace("Card UID: ", ""))
                })
            }
        })
    } catch (error) {
        statusDevice = "Fallo la conexion"
        console.log("2-Error al conectar con dispositivo")
    }
}

//BodyParser configuration
const bodyParser = require('body-parser')
    app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
    app.use(bodyParser.json({
    parameterLimit: 100000,
    limit: '10mb',
    extended: true
}))

//Routes definition
////////////////////////////////////////////////////////////////////////////////
app.get('/', (req, res) => {
    res.send('<h1>Service SISGPRO is running</h1>')
})
app.get('/getDevices', async (req, res) => {
    let devices = await getDeviceList()
    res.status(200).json(devices)
})

mongoose.connect('mongodb://localhost/sisgpro_presentismo', { useNewUrlParser: true })

//conexion a mongo
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Error al conectar con mongoDB:'))
db.once('open', function() {
    // io.emit('MONGO', 'Conectado')
})

io.on('connection', (socket) => {
    console.log('nueva conexion', socket.id)
    setInterval(()=>{
        io.emit('STATUS', { 
            device: device,
            statusDevice: statusDevice
        })
    }, 2000)

    io.sockets.emit('CONNECT','OK')
    // io.sockets.emit('DEVICE',{device: deviceConnect})
    socket.on('disconnect', function () {
        io.sockets.emit('DISCONNECT','OK')
    })
    socket.on('SET_DEVICE', async function(data){
        device = data.device
        conectarDispositivo()
    })


})

//Servers listeners
http.createServer(app).listen(3102)

console.log("SERVICE SISGPRO\n------------------\nHTTP Server PORT 3102")

getLastPort()
setInterval(() => enviarDatos(), 30000)